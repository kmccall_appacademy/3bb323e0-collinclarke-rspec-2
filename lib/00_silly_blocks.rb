def reverser
  yield.split.map do |word|
   word.reverse
  end.join(' ')
end

def adder(int=1)
  yield + int
end

def repeater(int=1)
 int.times do |n|
   yield
 end
end
